#include <stdio.h>      
#include <sys/socket.h> 
#include <arpa/inet.h>  
#include <stdlib.h>     
#include <string.h>     
#include <unistd.h>     
#include <time.h>
#include "protbuf.pb-c.h"

#define MAX_MSG_SIZE 1024
#define SIZE_BUF 255

void DieWithError(char *errorMessage);
int CreateUDPsocket(int port, const void *IP, struct sockaddr_in* broadcast);
int CreateTCPsocketClient(int port, const void *IP);