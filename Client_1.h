#include <stdio.h>      
#include <sys/socket.h> 
#include <arpa/inet.h>  
#include <stdlib.h>     
#include <string.h>     
#include <unistd.h>     
#include <time.h>
#include "protbuf.pb-c.h"

#define SIZE_BUF 255
#define MAX_DELAY 5
#define MIN_DELAY 1
#define MAX_LENGTH 12
#define MIN_LENGTH 1

char* random_string(char *str, int length);			 								
int CreateUDPsocket(int port, const void *IP, struct sockaddr_in* broadcast);  
void DieWithError(char *errorMessage);
int CreateTCPsocketClient(int port, const void *IP);  
