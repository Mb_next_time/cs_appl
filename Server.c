#include "Server.h"

int main(int argc, char *argv[]) {

    Q = createQueue();
    
    pthread_t threadUDP_1[NUMBER_CL], threadUDP_2[NUMBER_CL], threadTCP_1[NUMBER_CL], threadTCP_2[NUMBER_CL];
    struct Prot UDP_1[NUMBER_CL], UDP_2[NUMBER_CL], TCP_1[NUMBER_CL], TCP_2[NUMBER_CL];

    if (argc < 4) {       
        fprintf(stderr,"Usage:  %s <Port for client №1> <Port for client №2> <IP Broadcast>\n", argv[0]);
        exit(1);
    }

    int minport1 = atoi(argv[1]);
    int maxport1 = atoi(argv[1])+NUMBER_CL-1;

    if ((atoi(argv[2]) >= minport1) && (atoi(argv[2]) <= maxport1)) {
        printf("Для второго клиента используйте порты  не попадающие в диапазон: %d:%d\n", minport1, maxport1);
        exit(1);
    }

    int minport2 = atoi(argv[2]);
    int maxport2 = atoi(argv[2])+NUMBER_CL-1;

    if ((atoi(argv[1]) >= minport2) && (atoi(argv[1]) <= maxport2)) {
        printf("Для первого клиента используйте порты  не попадающие в диапазон: %d:%d\n", minport2, maxport2);
        exit(1);
    }

    int port1 = atoi(argv[1]);
    int port2 = atoi(argv[2]);

    for(int i = 0; i < NUMBER_CL; i++) {
        UDP_1[i].port = port1;
        TCP_1[i].port = port1;
        UDP_2[i].port = port2;
        TCP_2[i].port = port2;
        port1+=1;
        port2+=1;
        UDP_1[i].IP = argv[3];
        TCP_1[i].IP = argv[3];
        UDP_2[i].IP = argv[3];
        TCP_2[i].IP = argv[3];
    }

    for(int i = 0; i < NUMBER_CL; i++) {
        pthread_create(&threadUDP_1[i], NULL, UDP_Cl_1, &UDP_1[i]);
        pthread_create(&threadTCP_1[i], NULL, TCP_Cl_1, &TCP_1[i]);
        pthread_create(&threadUDP_2[i], NULL, UDP_Cl_2, &UDP_2[i]);
        pthread_create(&threadTCP_2[i], NULL, TCP_Cl_2, &TCP_2[i]);
    }

    for(int i = 0; i < NUMBER_CL; i++) {
        pthread_join(threadUDP_1[i], NULL);
        pthread_join(threadTCP_1[i], NULL);
        pthread_join(threadUDP_2[i], NULL);
        pthread_join(threadTCP_2[i], NULL);
    }

    return 0;
}

void *UDP_Cl_1(void *threadArgs) {   
                 
    int sock;
    struct sockaddr_in broadcast; 
    struct Prot *point = (struct Prot*)threadArgs;
    

    sock = CreateUDPsocket(point->port, point->IP, &broadcast);

    int broadcastPermission = 1;

    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission, sizeof(broadcastPermission)) < 0) {
        DieWithError("setsockopt() failed in UDP_Cl_1");
    } 

    for(;;) {
        if(Q->size < QUEUE_MAX) {
            char mes[] = "I'am waiting messages.";
            unsigned int length; 

            length = strlen(mes);

            printf("The server has sended the message \"%s\" on %s\n",mes,point->IP);
            if (sendto(sock, mes, length, 0, (struct sockaddr *)&broadcast, sizeof(broadcast)) != length) {
                DieWithError("sendto() sent a different number of bytes than expected in UDP_Cl_1");
            }
            sleep(delay_UDP);   
        }
        else {
            printf("The queue is full.\n");
            sleep(delay_UDP);
        }
    }
}

void *UDP_Cl_2(void *threadArgs) {   
    
    int sock;
    struct sockaddr_in broadcast;
    struct Prot *point = (struct Prot*)threadArgs;
    
    sock = CreateUDPsocket(point->port, point->IP, &broadcast);

    int broadcastPermission = 1;

    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission, sizeof(broadcastPermission)) < 0) {
        DieWithError("setsockopt() failed in UDP_Cl_1");
    }          

    for(;;) {
        if(Q->size > QUEUE_MIN) {
            char mes[] = "The queue has message.";
            unsigned int length;

            length = strlen(mes);

            printf("The server has sended the message \"%s\" on %s\n",mes,point->IP);
            if (sendto(sock, mes, length, 0, (struct sockaddr *)&broadcast, sizeof(broadcast)) != length) {
                DieWithError("sendto() sent a different number of bytes than expected in UDP_Cl_2");
            }
            sleep(delay_UDP);   
        }
        else {
            printf("The queue is empty.\n");
            sleep(delay_UDP);
        }
    }
}

void* TCP_Cl_1(void *threadArgs) {
    
    int sock;
    struct Prot *point = (struct Prot*)threadArgs;
    
    sock = CreateTCPServerSocket(point->port);

    for(;;) {            
        int clntSock;
        clntSock = AcceptTCPConnection(sock);
        size_t msg_len;
        uint8_t Protobuf[MAX_MSG_SIZE];
        Prot *msg;

        recv(clntSock, &msg_len, sizeof(size_t), 0);
        recv(clntSock, Protobuf, msg_len, 0);
        msg = prot__unpack(NULL, msg_len, Protobuf);
        struct ProtCl *buf = (struct ProtCl*)malloc(sizeof(struct ProtCl));
        buf->length = msg->length;
        buf->string = (char*)malloc(sizeof(char) * buf->length);
        strcpy(buf->string, msg->string);
        buf->Time = msg->time;
        push(Q, buf); 
        printf("Messages in the queue: %d\n\n", Q->size);  
        prot__free_unpacked(msg, NULL);
        close(clntSock);
        
    }
}

void* TCP_Cl_2(void *threadArgs) {
    
    int sock;
    struct Prot *point = (struct Prot*)threadArgs;
    

    sock = CreateTCPServerSocket(point->port);

    for(;;) {

        int clntSock;
        clntSock = AcceptTCPConnection(sock);
        struct ProtCl *buf = (struct ProtCl*)malloc(sizeof(struct ProtCl));
        buf = pop(Q);

        Prot msg = PROT__INIT;
        void *Protbuf;                     
        size_t len;
        msg.length = buf->length;
        msg.string = (char*)malloc(sizeof(char) * msg.length);
        strcpy(msg.string, buf->string);
        msg.time = buf->Time;
        len = prot__get_packed_size(&msg);
        send(clntSock, &len, sizeof(size_t), 0);
        Protbuf = malloc(len);
        prot__pack(&msg, Protbuf);
        send(clntSock, Protbuf, len, 0);
        printf("Messages in the queue: %d\n\n", Q->size);
        close(clntSock);
        free(buf);
        free(Protbuf);
    }
}