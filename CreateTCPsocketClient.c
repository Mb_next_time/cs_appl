#include <sys/socket.h> /* for socket(), bind(), and connect() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <string.h>     /* for memset() */

void DieWithError(char *errorMessage);

int CreateTCPsocketClient(int port, const void *IP) {

	int sock;
	struct sockaddr_in ServAddr;

	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
    	DieWithError("socket() failed Client");
    }

    memset(&ServAddr, 0, sizeof(ServAddr));     
    ServAddr.sin_family      = AF_INET;             
    ServAddr.sin_addr.s_addr = inet_addr(IP);   
    ServAddr.sin_port        = htons(port); 

    if (connect(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
    	DieWithError("connect() failed Client");
    }

	return sock;
}