#include "queue.h"

struct queue* createQueue() {
    struct queue *tmp = (struct queue*)malloc(sizeof(struct queue));
    tmp->size = 0;
    tmp->head = tmp->tail = NULL;
 
    return tmp;
}

void deleteQueue(struct queue **Q) {
    struct Node *tmp = (*Q)->head;
    struct Node *next = NULL;
    while (tmp) {
        next = tmp->next;
        free(tmp);
        tmp = next;
    }
    free(*Q);
    (*Q) = NULL;
}

void push(struct queue *Q, struct ProtCl *data) {
    struct Node *tmp = (struct Node*) malloc(sizeof(struct Node));
    if (tmp == NULL) {
        exit(1);
    }
    tmp->value = data;
    tmp->next = Q->head;
    tmp->prev = NULL;
    if (Q->head) {
        Q->head->prev = tmp;
    }
    Q->head = tmp;
    if (Q->tail == NULL) {
        Q->tail = tmp;
    }
    Q->size++;
}

void* pop(struct queue *Q) {
    struct Node *next;
    void *tmp;
    if (Q->tail == NULL) {
        exit(4);
    }
 
    next = Q->tail;
    Q->tail = Q->tail->prev;
    if (Q->tail) {
        Q->tail->next = NULL;
    }
    if (next == Q->head) {
        Q->head = NULL;
    }
    tmp = next->value;
    free(next);
 
    Q->size--;
    return tmp;
}

void display(struct queue *Q, void (*fun)(struct ProtCl*)) {
    struct Node *tmp = Q->head;
    while (tmp) {
        fun(tmp->value);
        tmp = tmp->next;
    }
    printf("\n");
}

void printStruct(struct ProtCl *value) {
    printf("%d: ", value->length);
    printf("%s ", value->string);
    printf("%d\n", value->Time);
}