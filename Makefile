Server Client_1 Client_2: CreateTCPServerSocket.c AcceptTCPConnection.c DieWithError.c random_string.c CreateUDPsocket.c CreateTCPsocketClient.c queue.c 
	protoc-c --c_out=. protbuf.proto
	gcc -std=c99 -o Server Server.c CreateTCPServerSocket.c AcceptTCPConnection.c DieWithError.c CreateUDPsocket.c queue.c -I. protbuf.pb-c.c -lprotobuf-c -lpthread
	gcc -std=c99 -o Client_1 Client_1.c  DieWithError.c random_string.c CreateUDPsocket.c CreateTCPsocketClient.c protbuf.pb-c.c -lprotobuf-c -I .
	gcc -std=c99 -o Client_2 Client_2.c  DieWithError.c CreateUDPsocket.c CreateTCPsocketClient.c protbuf.pb-c.c -lprotobuf-c -I.
clean:
	rm -f *.o Server Client_1 Client_2 protbuf.pb-c.c protbuf.pb-c.h