#include <sys/socket.h> /* for socket(), bind(), and connect() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <string.h>     /* for memset() */
#include <stdio.h>

void DieWithError(char *errorMessage);  						// Error handling function

int CreateUDPsocket(int port, const void *IP, struct sockaddr_in* broadcast) {

	int sock; 

    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        DieWithError("socket() failed in UDP_Cl_1");
    }

    memset(broadcast, 0, sizeof(*broadcast));
    broadcast->sin_family = AF_INET;
    if(IP == NULL) {
    	broadcast->sin_addr.s_addr = htonl(INADDR_ANY); 
    }
    else {
    	broadcast->sin_addr.s_addr = inet_addr(IP);
    }
    
    broadcast->sin_port = htons(port);

	return sock;
} 