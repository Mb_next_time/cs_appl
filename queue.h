#include <stdlib.h>
#include <malloc.h>
#include "structProt.h" 

struct Node {										
    void *value;
    struct Node *next;
    struct Node *prev; 
};

struct queue {
    size_t size;
    struct Node *head;
    struct Node *tail;
};

struct queue* createQueue();
void deleteQueue(struct queue **Q);
void push(struct queue *Q, struct ProtCl *data);
void* pop(struct queue *Q);
void display(struct queue *Q, void (*fun)(struct ProtCl*));
void printStruct(struct ProtCl *value);