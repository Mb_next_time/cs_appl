#include <stdio.h>     
#include <stdlib.h> 
#include <string.h>

char* random_string(char *str, int length) {

    char simbols[] = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz123456789!@#$%^&*()_+{}?><:";

    for(int i = 0; i < length; i++) {
        int k = rand() % strlen(simbols);
        str[i] = simbols[k];
    }

    return str;
}