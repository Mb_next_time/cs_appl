#include "Client_2.h"     

int main(int argc, char *argv[]) {
    
    if (argc != 2) {
        fprintf(stderr,"Usage: %s <Port>\n", argv[0]);
        exit(1);
    }

    unsigned short port;

    port = atoi(argv[1]);

    int sockUDP;
    struct sockaddr_in broadcastAddr;

    sockUDP = CreateUDPsocket(port, NULL, &broadcastAddr); 
   
    if (bind(sockUDP, (struct sockaddr *) &broadcastAddr, sizeof(broadcastAddr)) < 0) {
        DieWithError("bind() failed in Client №2");
    }

    struct sockaddr_in fromAddr;
    unsigned int fromAddrsize;

    fromAddrsize = sizeof(fromAddr);

    for(;;) {
        char advice[SIZE_BUF];
        int length_of_advice;

        if ((length_of_advice = recvfrom(sockUDP, advice, SIZE_BUF, 0, (struct sockaddr *)&fromAddr, &fromAddrsize)) < 0) {
            DieWithError("recvfrom() failed Client №2");
        }
        else {                        
            char* IP = inet_ntoa(fromAddr.sin_addr);
            advice[length_of_advice] = '\0';

            printf("The client №2 has accepted the message \"%s\" by %s(Server)\n",advice,IP);

            int sockTCP; 

            sockTCP = CreateTCPsocketClient(port, IP);    

            size_t msg_len;
            uint8_t buf[MAX_MSG_SIZE];
            Prot *msg;

            recv(sockTCP, &msg_len, sizeof(size_t), 0);
            recv(sockTCP, buf, msg_len, 0);
            msg = prot__unpack(NULL, msg_len, buf);
            msg->string[msg->length] = '\0';
            printf("The client №2 has accepted the message: %s\n", msg->string);
            int delay = msg->time;
            prot__free_unpacked(msg, NULL);
            close(sockTCP);
            sleep(delay);
        }
    }
}