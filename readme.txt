########################################################
#                                                      #
#				README FILE                        	   #
#                                                      #
########################################################

Для пересборки проекта используйте remake.sh.
Для автозапуска используйте run.sh.
Изначальное кол-во подключаемых клиентов 1, в файле Server.h измените переменную NUMBER_CL для нужного кол-ва подключаемых клиентов. 
Правила запуска испольняемых файлов:
- ./Server 8000(порт для клиента №1) 8001(порт для клиента №2) 192.168.1.255(broadcast);
- ./Client_1 8000(порт для соединения с сервером);
- ./Client_2 8001(порт для соединения с сервером).