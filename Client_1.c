#include "Client_1.h" 

int main(int argc, char *argv[]) {
                            
    if (argc != 2) {
        fprintf(stderr,"Usage: %s <Port>\n", argv[0]);
        exit(1);
    }

    srand(time(NULL));

    unsigned short port;

    port = atoi(argv[1]);  

    int sockUDP;
    struct sockaddr_in broadcastAddr;

    sockUDP = CreateUDPsocket(port, NULL, &broadcastAddr); 

    if (bind(sockUDP, (struct sockaddr *) &broadcastAddr, sizeof(broadcastAddr)) < 0) {
        DieWithError("bind() failed Client №1");
    }

    struct sockaddr_in fromAddr;
    unsigned int fromAddrsize;

    fromAddrsize = sizeof(fromAddr);

    for(;;) {
        char advice[SIZE_BUF];            
        int length_of_advice;

        if ((length_of_advice = recvfrom(sockUDP, advice, SIZE_BUF, 0, (struct sockaddr *)&fromAddr, &fromAddrsize)) < 0) {
            DieWithError("recvfrom() failed Client №1");
        }
        else {
            int sockTCP;
            struct sockaddr_in ServAddr;

            char* IP = inet_ntoa(fromAddr.sin_addr);

            advice[length_of_advice] = '\0';

            printf("The client №1 has accepted the message \"%s\" by %s(Server)\n",advice,IP);    

            sockTCP = CreateTCPsocketClient(port, IP); 

            Prot msg = PROT__INIT;

            char *string;

            void *Protbuf;                     
            size_t len;

            msg.length = rand() % MAX_LENGTH + MIN_LENGTH;
            string = (char*)malloc(sizeof(char) * msg.length);
            string = random_string(string, msg.length);     
            string[msg.length] = '\0';
            msg.string = (char*)malloc(sizeof(char) * msg.length);
            strcpy(msg.string, string);
            printf("The client №1 has sended the message: %s\n", msg.string);
            msg.time = rand() % MAX_DELAY + MIN_DELAY;
            int delay = msg.time;
            len = prot__get_packed_size(&msg);
            send(sockTCP, &len, sizeof(size_t), 0);
            Protbuf = malloc(len);
            prot__pack(&msg, Protbuf);
            send(sockTCP, Protbuf, len, 0);
            free(string);
            free(Protbuf);
            close(sockTCP);
            sleep(delay);
        }
    }
}
